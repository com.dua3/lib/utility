// Copyright (c) 2019 Axel Howind
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

open module com.dua3.utility.swing.test {
    exports com.dua3.utility.swing.test;
    
    requires com.dua3.utility;
    requires com.dua3.utility.logging;
    requires com.dua3.utility.swing;
    requires org.apache.logging.log4j;
    requires org.slf4j;
    requires jul.to.slf4j;
    requires java.desktop;
    requires java.logging;
}
